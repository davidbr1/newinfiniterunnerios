﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEditor;

namespace MoreMountains.Tools
{	

	[CustomPropertyDrawer(typeof(HiddenAttribute))]
	/**
	* ReadOnlyAttributeDrawer - A class to make Read-Only inspector properties.
	**/
	public class HiddenAttributeDrawer : PropertyDrawer
	{
	    // Necessary since some properties tend to collapse smaller than their content
	    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	    {
	    	return 0f;
	        //return EditorGUI.GetPropertyHeight(property, label, true);
	    }

	    // Draw a disabled property field
	    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	    {
	       /* GUI.enabled = false; // Disable fields
	        EditorGUI.PropertyField(position, property, label, true);
	        GUI.enabled = true; // Enable fields*/
	    }
	}
}