﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class CannonballSpawnerHard : MonoBehaviour {
		
		public GameObject cannonball;

		//Exclamation Mark
		public GameObject exMark;
		public GameObject canvas;
		//Spawn positions
		public float[] spawnPositions;
		public float initialDelay;
		public float breakTime;
		public int numberOfBalls;

		//Debugging Only
		public int lastSpawnPositionNumber = 99;
		//Choose Random Y-Position Item and set it as last spawn position
		private int spawnPositionNumber1;
		private int spawnPositionNumber2;

		//Size of array equals number of balls to spawn
		private int[] positionsToSpawn;


		// Use this for initialization
		void Start () {
			positionsToSpawn = new int[numberOfBalls];
			//Invoke at delay
			Invoke ("SpawnBall", initialDelay);
				

		}


		void SpawnBall() {	
			//Only Spawn while game in progress (not on pause, not during game over screen)
			if (GameManager.Instance.Status == GameManager.GameStatus.GameInProgress) {

				//Choose Random time in between range for the next spawn for all balls and fill in array;
				for (int i = 0; i < numberOfBalls; i++) {
				
					int randomPos = Random.Range (0, spawnPositions.Length);
					positionsToSpawn [i] = randomPos;
				
					lastSpawnPositionNumber = positionsToSpawn [i];
				}


				//Show Excl Mark and Cannonball
				StartCoroutine (ShowExMark (positionsToSpawn));

		

				Invoke ("SpawnBall", breakTime);
			} 
		}

		//Function to show Exclamation Mark Warning before cannonball
		IEnumerator ShowExMark(int[] spawnpositionsActual){	

			foreach (int position in spawnpositionsActual) {
				GameObject exMarkUi = Instantiate(exMark, transform.position, Quaternion.identity) as GameObject;

				exMarkUi.transform.SetParent (canvas.transform, false);
				RectTransform rectTransform = exMarkUi.GetComponent<RectTransform> ();
				/*rectTransform.pivot = new Vector2 (0.5f, 0.5f);
			rectTransform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);
			rectTransform.anchorMin = new Vector2 (0, 0);
			rectTransform.anchorMax = new Vector2 (0, 0);*/
				

				if (spawnPositions[position] == -13) {
					rectTransform.anchoredPosition = new Vector2 (125, -47);			
				} else if (spawnPositions[position] == -12) {
					rectTransform.anchoredPosition = new Vector2 (125, -32);					
				} else if (spawnPositions[position] == -11) {
					rectTransform.anchoredPosition = new Vector2 (125, -17);	
				} else if (spawnPositions[position] == -10) {
					rectTransform.anchoredPosition = new Vector2 (125, -2);	
				} else if (spawnPositions[position] == -9) {
					rectTransform.anchoredPosition = new Vector2 (125, 13);	
				} else if (spawnPositions[position] == -8) {
					rectTransform.anchoredPosition = new Vector2 (125, 28);	
				} else if (spawnPositions[position] == -7) {
					rectTransform.anchoredPosition = new Vector2 (125, 43);	
				} else if (spawnPositions[position] == -6) {
					rectTransform.anchoredPosition = new Vector2 (125, 58);	
				}
				GetComponent<AudioSource> ().Play ();
				yield return new WaitForSeconds (0.5f);
				exMarkUi.SetActive (false);
				yield return new WaitForSeconds (0.5f);
				GetComponent<AudioSource> ().Play ();
				exMarkUi.SetActive (true);
				yield return new WaitForSeconds (0.5f);
				exMarkUi.SetActive (false);
				yield return new WaitForSeconds (0.5f);
				GetComponent<AudioSource> ().Play ();
				exMarkUi.SetActive (true);
				yield return new WaitForSeconds (0.5f);
				Destroy (exMarkUi);
				yield return new WaitForSeconds (1f);
				Instantiate (cannonball, new Vector2(transform.position.x, spawnPositions[position] ), Quaternion.identity);
			
			}

		}
	}
}