﻿using UnityEngine;
using System.Collections;

namespace MoreMountains.InfiniteRunnerEngine {
	public class ConstantRotation : MonoBehaviour {

		public float rotationValue;
		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			transform.Rotate (0,0,rotationValue*Time.deltaTime); //rotates x degrees per second around z axis	
		}
	}
}
